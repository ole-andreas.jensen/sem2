# Mitt program

I denne semesteroppgaven har jeg laget et *HANGMAN*-spill. Selve spillet følger en rimelig klassisk oppskrift når det kommer til grafisk utforming samt gameplay. 
Men, i lys av såkalt *GENDER EQUITY* har jeg kalt spillet mitt *HANGTHEM*, for å oppnå et kjønnsnøytralt utgangspunkt som passer for alle.
Representation matters (!!!!) - spesielt når det kommer til 2D-dataspill hvor man henger *strekHEN*.

Spillet har nøyaktig samme gameplay som vanlige hangman-spill.
Kort forklart:
* "Inni" underscoresene gjemmer det seg et ord
* Dette ordet skal man gjette ved å trykke på taster på tastaturet
* Gjetter man *riktig* dukker bokstaven(e) opp i ordet/erstatter underscorene
* Gjetter man *feil* tegnes én og én kroppsdel på figuren i galgen
* Gjetter man *feil mer enn 6 ganger* taper man
* Man kan restarte spillet ved å trykke "R" (for "Restart")


Se følgende lenke for demovideo:
https://www.youtube.com/shorts/qRFi3zxxtNE



* Ordene hentes fra filen *nsf2022*, som er Norsk Scrabbleforbunds liste over godkjente Scrabble-ord i 2022

* All JAVADOC er skrevet av chatGPT *

* Musikk komponert av meg *

