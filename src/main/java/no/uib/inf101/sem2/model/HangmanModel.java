package no.uib.inf101.sem2.model;

import java.util.ArrayList;
import java.util.Random;

public class HangmanModel {

  // Field variable(s)
  private GameState gameState = GameState.ACTIVE_GAME;
  private fileProcessor fileProcessor;
  public ArrayList<String> gameWord;
  private char[] secretWord;
  private ArrayList<Character> guessedLetters;
  int wrongGuesses = 0;
  private char[] underscoreString;

  /**
   * Constructs a new instance of the HangmanModel class with the specified secret
   * word.
   *
   * @param secretWord the secret word to be guessed in the game, represented as a
   *                   String
   */
  public HangmanModel(String secretWord) {
    this.gameState = GameState.ACTIVE_GAME;
    this.fileProcessor = new fileProcessor();
    this.gameWord = fileProcessor.wordlistFromFile("./src/main/resources/nsf2022.txt");
    this.secretWord = secretWord == null ? findSecretWord(gameWord) : secretWord.toCharArray();
    this.underscoreString = getUnderscoreString();
    this.guessedLetters = new ArrayList<>();
  }

  /**
   * Selects a random word from the provided list and returns its characters as a
   * character array.
   * 
   * @param gameWord2 An ArrayList of strings containing the candidate words to
   *                  choose from.
   * @return A character array representing the characters of the selected word.
   */
  private char[] findSecretWord(ArrayList<String> gameWord2) {
    Random random = new Random();
    int randomIndex = random.nextInt(gameWord2.size());
    return gameWord2.get(randomIndex).toCharArray();
  }

  /**
   * @return gameState
   */
  public GameState getGameState() {
    return gameState;
  }

  /**
   * Checks if the provided letter matches any of the characters in the secret
   * word. If there is a match,
   * the corresponding underscore in the underscore string is replaced with the
   * letter. If there is no match,
   * the number of wrong guesses is incremented and the game state is checked to
   * see if the game is over.
   *
   * @param key The letter to check against the secret word.
   * @return True if the letter matches a character in the secret word, false
   *         otherwise.
   */
  public boolean guessLetter(char key) {
    if (Character.isLetter(key)) {
      if (letterAlreadyGuessed(key)) {
        return true;
      }
      guessedLetters.add(key);
      for (int i = 0; i < secretWord.length; i++) {
        if (secretWord[i] == key) {
          updateUnderscoreString(key);
          updateGameState();
          return true;
        }
      }
      wrongGuesses++;
      updateGameState();
    }
    return false;
  }

  /**
   * Updates the underscoreString array by replacing underscores with the guessed
   * character if it exists in the secretWord array.
   * 
   * @param key the character guessed by the user
   */
  private void updateUnderscoreString(char key) {
    for (int i = 0; i < secretWord.length; i++) {
      if (secretWord[i] == key) {
        underscoreString[i] = secretWord[i];
      }
    }
  }

  /**
   * Updates the game state based on whether the game is over or won.
   * If the game is over, sets the gameState to GAME_OVER and displays the secret
   * word.
   * If the game is won, sets the gameState to GAME_WON.
   */

  private void updateGameState() {
    if (checkIfGameOver()) {
      displaySecretWord();
      gameState = GameState.GAME_OVER;
    } else if (checkIfGameWon()) {
      gameState = GameState.GAME_WON;
    }
  }

  /**
   * Checks if a letter has already been guessed by the player.
   * 
   * @param key The letter to check for previous guesses.
   * @return true if the letter has already been guessed, false otherwise.
   */

  private boolean letterAlreadyGuessed(Character key) {
    if (guessedLetters.contains(key)) {
      return true;
    }
    return false;
  }

  /**
   * Checks if the game is over by comparing the number of wrong guesses
   * with the maximum allowed guesses (6 by default).
   *
   * @return {@code true} if the game is over, {@code false} otherwise.
   */

  private boolean checkIfGameOver() {
    return wrongGuesses == 6;
  }

  /**
   * Checks if the game has been won by comparing the guessed word
   * represented by the underscores (underscoreString) with the secret word.
   *
   * @return {@code true} if the game has been won, {@code false} otherwise.
   */

  private boolean checkIfGameWon() {
    for (int i = 0; i < secretWord.length; i++) {
      if (underscoreString[i] != secretWord[i]) {
        return false;
      }
    }
    return true;
  }

  /**
   * @returns the *int value* of the wrongGuesses variable
   */
  public int getWrongGuesses() {
    return wrongGuesses;
  }

  /**
   * Returns a character array with the same length as the secret word, where each
   * character is an underscore '_'.
   *
   * @return A character array containing underscores, with the same length as the
   *         secret word.
   */
  public char[] getUnderscoreString() {
    char[] underscoreString = new char[secretWord.length];
    for (int i = 0; i < secretWord.length; i++) {
      underscoreString[i] = '_';
    }
    return underscoreString;
  }

  /**
   * Returns the list of words used in the game.
   * 
   * @return An ArrayList of strings containing the words used in the game.
   */
  public ArrayList<String> getGameWord() {
    return gameWord;
  }

  /**
   * Returns a string representation of the current state of the game, where each
   * character in the secret word is
   * replaced with an underscore if it has not been guessed yet.
   *
   * @return A string containing underscores and the letters that have been
   *         correctly guessed so far.
   */
  public String getDisplayString() {
    String displayString = "";
    for (int i = 0; i < secretWord.length; i++) {
      displayString += underscoreString[i] + " ";
    }
    return displayString;
  }

  /**
   * Updates the underscore string to reveal the entire secret word. This method
   * is called when the player has
   * exceeded the maximum number of allowed wrong guesses.
   */
  private void displaySecretWord() {
    for (int i = 0; i < secretWord.length; i++) {
      underscoreString[i] = secretWord[i];
    }
  }

  /**
   * Restarts the game by resetting the game state, retrieving a new secret word
   * from the wordlist,
   * initializing the underscore string for displaying the secret word, and
   * resetting the guessed letters
   * and wrong guesses count.
   */
  public void restartGame() {
    this.gameState = GameState.ACTIVE_GAME;
    this.fileProcessor = new fileProcessor();
    this.gameWord = fileProcessor.wordlistFromFile("./src/main/resources/nsf2022.txt");
    this.secretWord = findSecretWord(gameWord);
    this.underscoreString = getUnderscoreString();
    this.guessedLetters = new ArrayList<>();
    wrongGuesses = 0;
  }

}
