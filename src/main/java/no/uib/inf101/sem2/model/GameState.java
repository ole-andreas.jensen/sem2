package no.uib.inf101.sem2.model;

/**
 * An enum representing the possible states of a game.
 *
 * <p>The enum has two possible values: ACTIVE_GAME and GAME_OVER. ACTIVE_GAME represents a game that is still
 * in progress, while GAME_OVER represents a game that has ended.</p>
 */
public enum GameState {
    ACTIVE_GAME,
    GAME_OVER,
    GAME_WON,
}
