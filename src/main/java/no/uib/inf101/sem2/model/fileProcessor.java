package no.uib.inf101.sem2.model;

import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;
import java.io.FileReader;

public class fileProcessor {

    /**
     * Reads a file containing the legal scrabble words from Norsk Scrabbleforbunds 2022 list.
     * ArrayList of strings.
     *
     * @param filename "nsf2022.txt"
     * @return An ArrayList of strings containing the contents of the file.
     */
    public ArrayList<String> wordlistFromFile(String filename) {
        ArrayList<String> words = new ArrayList<String>();
        try {
            File file = new File(
                    "./src/main/resources/nsf2022.txt");
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                while (br.readLine() != null) {
                    words.add(br.readLine());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return words;
    }

}
