package no.uib.inf101.sem2.wav;

import javax.sound.sampled.*;


public class WavPlayer {

    /**
     * Plays a .wav file.
     *
     * @param filePath The path to the .wav file to be played.
     * @throws UnsupportedAudioFileException If the audio file format is not
     *                                       supported.
     * @throws LineUnavailableException      If a line to the audio system is
     *                                       unavailable.
     * @throws InterruptedException          If the thread is interrupted while
     *                                       waiting for the clip to finish playing.
     * @throws IOException                   If an I/O error occurs while reading
     *                                       the audio file.
     * THIS METHOD WAS WRITTEN BY ChatGPT
     */
    public static void playWavFile(String filePath) {
        try {
            // Open the .wav file
            AudioInputStream audioInputStream = AudioSystem
                    .getAudioInputStream(WavPlayer.class.getResourceAsStream("/updatedhangmanmusic.wav"));
            AudioFormat format = audioInputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip clip = (Clip) AudioSystem.getLine(info);

            // Load the .wav file into the clip
            clip.open(audioInputStream);

            // Start playing the clip
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);

        } catch (UnsupportedAudioFileException | LineUnavailableException
                | java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
