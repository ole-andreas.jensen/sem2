package no.uib.inf101.sem2.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.QuadCurve2D;
import javax.swing.JPanel;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.HangmanModel;

public class HangmanView extends JPanel {
  /**
   * Field Variable(s)
   */
  private HangmanModel model;

  /**
   * Constructor
   */
  public HangmanView(HangmanModel model) {
    this.model = model;
    this.setPreferredSize(new Dimension(800, 800));
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.setStroke(new BasicStroke(15));
    this.drawGallows(g2);
    this.drawNext(g2);
    printIfGameWon(g2);
    displayUnderscoreString(g2);
  }

  /**
   * Draws the next state of the game on the graphics context.
   * This method checks the current game state and calls the appropriate method
   * to draw the corresponding game state (either alive person or dead person).
   *
   * @param g2 The Graphics2D object to draw on.
   */
  private void drawNext(Graphics2D g2) {
    if (model.getGameState() != GameState.GAME_OVER) {
      this.drawAlivePerson(g2);
    } else if (model.getGameState() == GameState.GAME_OVER) {
      this.drawDeadPerson(g2);
    }
  }

  /**
   * draws the ground + gallows + rope
   * if GAMESTATE == GAME_OVER -> calls drawDeadPerson()
   * if GAMESTATE == GAME_ACTIVE -> calls drawAlivePerson()
   * 
   * @param g2
   */
  private void drawGallows(Graphics2D g2) {
    g2.setFont(new Font("Arial", Font.BOLD, 18));
    g2.setColor(Color.BLACK);
    Inf101Graphics.drawCenteredString(g2, "Welcome to HangTHEM - the GENDER NEUTRAL Hangman game", this.getWidth() / 2,
        this.getHeight() * ((double) 1 / 18));
    Inf101Graphics.drawCenteredString(g2, "Why? Because gender representation is SUPERDUPERMEGA important!!!!!!!!",
        this.getWidth() / 2,
        this.getHeight() * ((double) 1 / 10));
    g2.setColor(Color.BLACK);
    g2.setStroke(new BasicStroke(15));
    Line2D ground = new Line2D.Double(100, 600, 700, 600);
    Line2D gallowsUp = new Line2D.Double(200, 200, 200, 600);
    Line2D gallowsRight = new Line2D.Double(200, 200, 425, 200);
    Line2D ropeDown = new Line2D.Double(428, 200, 428, 290);
    g2.draw(ground);
    g2.setStroke(new BasicStroke(10));
    g2.draw(gallowsUp);
    g2.draw(gallowsRight);
    g2.setStroke(new BasicStroke(4));
    g2.draw(ropeDown);
    g2.setStroke(new BasicStroke(2));
  }

  /**
   * Draws an alive person using Graphics2D object, based on the number of wrong
   * guesses in the game.
   * The person is drawn using various shapes, including Ellipse2D for the head
   * and eyes,
   * Line2D for the body, arms, legs, and mouth.
   * 
   * @param g2 The Graphics2D object to be used for drawing the person.
   */
  private void drawAlivePerson(Graphics2D g2) {
    Ellipse2D head = new Ellipse2D.Double(387, 290, 80, 70);
    Line2D body = new Line2D.Double(428, 360, 428, 500);
    Line2D leftArm = new Line2D.Double(428, 400, 395, 450);
    Line2D rightArm = new Line2D.Double(428, 400, 460, 450);
    Line2D leftLeg = new Line2D.Double(428, 500, 400, 550);
    Line2D rightLeg = new Line2D.Double(428, 500, 460, 550);
    Ellipse2D aliveLeftEye = new Ellipse2D.Double(405, 310, 10, 10);
    Ellipse2D aliveRightEye = new Ellipse2D.Double(437, 310, 10, 10);
    Line2D aliveMouth = new Line2D.Double(418, 340, 435, 340);
    int wrongGuesses = model.getWrongGuesses();
    if (wrongGuesses >= 1) {
      g2.setStroke(new BasicStroke(4));
      g2.draw(head);
      if (model.getGameState() != GameState.GAME_OVER) {
        g2.setStroke(new BasicStroke(2));
        g2.draw(aliveLeftEye);
        g2.draw(aliveRightEye);
        g2.setStroke(new BasicStroke(3));
        g2.draw(aliveMouth);
      }
    }
    if (wrongGuesses >= 2) {
      g2.draw(body);
    }
    if (wrongGuesses >= 3) {
      g2.draw(leftArm);
    }
    if (wrongGuesses >= 4) {
      g2.draw(rightArm);
    }
    if (wrongGuesses >= 5) {
      g2.draw(leftLeg);
    }
    if (wrongGuesses >= 6) {
      g2.draw(rightLeg);
    }
  }

  /**
   * Draws a dead person using Graphics2D object, based on the predefined
   * coordinates and shapes.
   * The person is drawn using various shapes, including Ellipse2D for the head,
   * Line2D for the body, arms, and legs, and QuadCurve2D for the eyes and mouth.
   * Additionally, the method sets the font, color, and displays a game over
   * message using the
   * Inf101Graphics.drawCenteredString() method.
   * 
   * @param g2 The Graphics2D object to be used for drawing the dead person.
   */
  private void drawDeadPerson(Graphics2D g2) {
    Ellipse2D head = new Ellipse2D.Double(387, 290, 80, 70);
    Line2D body = new Line2D.Double(428, 360, 428, 500);
    Line2D leftArm = new Line2D.Double(428, 400, 395, 450);
    Line2D rightArm = new Line2D.Double(428, 400, 460, 450);
    Line2D leftLeg = new Line2D.Double(428, 500, 400, 550);
    Line2D rightLeg = new Line2D.Double(428, 500, 460, 550);
    Line2D deadLeftEye1 = new Line2D.Double(410, 310, 420, 320);
    Line2D deadLeftEye2 = new Line2D.Double(410, 320, 420, 310);
    Line2D deadRightEye1 = new Line2D.Double(435, 310, 445, 320);
    Line2D deadRightEye2 = new Line2D.Double(435, 320, 445, 310);
    QuadCurve2D deadMouth = new QuadCurve2D.Double(415, 340, 426, 320, 435, 340);
    g2.setStroke(new BasicStroke(4));
    g2.draw(head);
    g2.draw(body);
    g2.draw(leftArm);
    g2.draw(rightArm);
    g2.draw(leftLeg);
    g2.draw(rightLeg);
    g2.setStroke(new BasicStroke(2));
    g2.draw(deadLeftEye1);
    g2.draw(deadLeftEye2);
    g2.draw(deadRightEye1);
    g2.draw(deadRightEye2);
    g2.setStroke(new BasicStroke(3));
    g2.draw(deadMouth);
    g2.setFont(new Font("Arial", Font.ITALIC, 25));
    g2.setColor(Color.RED);
    Inf101Graphics.drawCenteredString(g2, "GAME OVER! READ MORE BOOKS!", this.getWidth() / 2,
        this.getHeight() * ((double) 12 / 13));

    g2.setColor(Color.BLACK);
    Inf101Graphics.drawCenteredString(g2, "Press the SPACE BAR to play again", this.getWidth() / 2,
        this.getHeight() * ((double) 22 / 23));
  }

  /**
   * Displays an underscore string using the Graphics2D object, based on the
   * current display string
   * from the model. The underscore string is centered horizontally and positioned
   * near the bottom of the drawing area.
   * 
   * @param g2 The Graphics2D object to be used for displaying the underscore
   *           string.
   */
  private void displayUnderscoreString(Graphics2D g2) {
    g2.setColor(Color.BLACK);
    g2.setFont(new Font("Monospaced", Font.BOLD, 40));
    Inf101Graphics.drawCenteredString(g2, model.getDisplayString(), this.getWidth() / 2,
        this.getHeight() * ((double) 5 / 6));
  }

  /**
   * Prints the "game won" message on the specified Graphics2D object if the game
   * has
   * been won, based on the current game state from the model.
   * 
   * If the game state from the model is set to {@link GameState#GAME_WON}, this
   * method
   * will set the font, color, and position of the "game won" message on the
   * Graphics2D
   * object. The message "You won! Great Job!" will be printed centered
   * horizontally at
   * the middle of the window's width and at the 12/13th position of the window's
   * height.
   * Additionally, the message "Press 'R' to play again" will be printed centered
   * horizontally at the middle of the window's width and at the 22/23rd position
   * of the
   * window's height.
   * 
   * @param g2 The Graphics2D object on which to print the "game won" message.
   */
  private void printIfGameWon(Graphics2D g2) {
    if (model.getGameState() == GameState.GAME_WON) {
      g2.setFont(new Font("Arial", Font.ITALIC, 25));
      g2.setColor(Color.RED);
      Inf101Graphics.drawCenteredString(g2, "You won! Great Job!", this.getWidth() / 2,
          this.getHeight() * ((double) 12 / 13));

      Inf101Graphics.drawCenteredString(g2, "Press the SPACE BAR to play again", this.getWidth() / 2,
          this.getHeight() * ((double) 22 / 23));
    }
  }
}
