package no.uib.inf101.sem2;

import no.uib.inf101.sem2.view.HangmanView;
import no.uib.inf101.sem2.wav.WavPlayer;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import no.uib.inf101.sem2.controller.HangmanController;
import no.uib.inf101.sem2.model.HangmanModel;

public class Main {
  public static void main(String[] args) throws FileNotFoundException {
    HangmanModel model = new HangmanModel(null);
    HangmanView view = new HangmanView(model);
    new HangmanController(model, view);
    WavPlayer.playWavFile(null);
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setResizable(false);
    frame.setTitle("HANGTHEM - THE GENDER NEUTRAL HANGMAN GAME");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}


