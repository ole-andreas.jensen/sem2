package no.uib.inf101.sem2.controller;

import java.awt.event.KeyEvent;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.HangmanModel;
import no.uib.inf101.sem2.view.HangmanView;

public class HangmanController implements java.awt.event.KeyListener {

    /** Field variables */
    private HangmanModel model;
    private HangmanView view;

    /** Constructor */
    public HangmanController(HangmanModel model, HangmanView view) {
        this.view = view;
        this.model = model;
        view.addKeyListener(this);
        view.setFocusable(true);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        char key = e.getKeyChar();
        if (model.getGameState() == GameState.ACTIVE_GAME) {
            model.guessLetter(Character.toLowerCase(key));
        }
        view.repaint();
        if (model.getGameState() != GameState.ACTIVE_GAME) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                model.restartGame();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
}