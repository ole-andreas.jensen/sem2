package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.HangmanModel;

public class TestHangmanModel {
    
    @Test
    public void demo() {
        HangmanModel model = new HangmanModel("testtest");
        assertFalse(model.guessLetter('x'));
        assertFalse(model.getGameState() == GameState.GAME_OVER);

        assertFalse(model.guessLetter('z'));
        assertFalse(model.guessLetter('y'));
        assertFalse(model.guessLetter('q'));
        assertFalse(model.guessLetter('w'));
        assertFalse(model.getGameState() == GameState.GAME_OVER);

        assertFalse(model.guessLetter('r'));
        assertTrue(model.getGameState() == GameState.GAME_OVER);
    }


    @Test
    public void testgetUnderscoreString() {
        String testString = "ferdigmedsemesteroppgavetohurra";
        HangmanModel model = new HangmanModel(testString);
        char[] underscoreArray = model.getUnderscoreString();

        int expectedLength = testString.length();
        int actualLength = underscoreArray.length;

        assertEquals(expectedLength, actualLength);
            


        }
    }

